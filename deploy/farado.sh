#!/bin/sh

### BEGIN INIT INFO
# Provides:             farado
# Required-Start:       $network $local_fs $remote_fs
# Required-Stop:        $network $local_fs $remote_fs
# Default-Start:        2 3 4 5
# Default-Stop:         0 1 6
# Short-Description:    Farado is a web-based project management tool
# Description:          Farado is a free and open source, web-based project
#                       management and issue tracking tool
### END INIT INFO

PATH="/sbin:/bin:/usr/sbin:/usr/bin"
NAME="farado_main"

APPLICATION_PATH="/usr/local/bin/farado/"
CURRENT_PATH=$PWD

test -x $APPLICATION_PATH || exit 0

set -e

case "${1}" in
    start)
        cd $APPLICATION_PATH
        python $NAME.py &
        cd $CURRENT_PATH
        ;;

    stop)
        ps ax | grep $NAME.py | grep -v grep | awk '{print $1}' | xargs kill -9
        ;;

    restart|force-reload)
        ps ax | grep $NAME.py | grep -v grep | awk '{print $1}' | xargs kill -9

        cd $APPLICATION_PATH
        python $NAME.py &
        cd $CURRENT_PATH
        ;;

    *)
        echo "Usage: ${0} {start|stop|restart|force-reload}" >&2
        exit 1
        ;;
esac

exit 0
