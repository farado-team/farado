
## Развёртывание системы

### Ubuntu

#### Установите python

```
sudo apt install python3
sudo apt install python-is-python3
sudo apt install pip
```


#### Разместите Farado в нужном месте

Выполните копирование файлов Farado в `/usr/local/bin/farado/` или измените
значение переменной `APPLICATION_PATH` в `farado.sh` в соответствии с новым
местоположением Farado.


#### Установите зависимости находясь в корне Farado

```
sudo pip install -r requirements.txt
```


#### Создайте БД

```
sudo python ./default_data_creator.py
```


#### Установите инициализирующий скрипт

Находясь в корне Farado запустите команды:

```
sudo cp deploy/farado.sh /etc/init.d/
sudo chmod +x /etc/init.d/farado.sh
sudo update-rc.d farado.sh defaults
```

Farado будет запущена автоматически после перезагрузки ОС.

Для запуска/останова/перезапуска Farado можно воспользоваться командами:

```
sudo /etc/init.d/farado.sh start
sudo /etc/init.d/farado.sh stop
sudo /etc/init.d/farado.sh restart
```
