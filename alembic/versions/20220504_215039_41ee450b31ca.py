"""Добавлена связь таблицы файлов с комментариями.

Revision ID: 41ee450b31ca
Revises: 
Create Date: 2022-05-04 21:50:39.289917

"""
from alembic import op
import sqlalchemy


# revision identifiers, used by Alembic.
revision = '41ee450b31ca'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('files') as batch_op:
        batch_op.add_column(
            sqlalchemy.Column('comment_id', sqlalchemy.INTEGER, index=True)
        )
        batch_op.create_foreign_key(
            constraint_name='fk_file_comment',
            referent_table='comments',
            local_cols=['comment_id'],
            remote_cols=['id'],
            ondelete='CASCADE',
        )


def downgrade():
    pass
