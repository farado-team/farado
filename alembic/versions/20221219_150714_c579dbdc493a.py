"""Добавлен производственный календарь

Revision ID: c579dbdc493a
Revises: 0dfc2a9b7765
Create Date: 2022-12-19 15:07:14.694012

"""
from alembic import op
import sqlalchemy


# revision identifiers, used by Alembic.
revision = 'c579dbdc493a'
down_revision = '0dfc2a9b7765'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'standard_days',
        sqlalchemy.Column(
            'id',
            sqlalchemy.INTEGER,
            primary_key=True,
            index=True,
        ),
        sqlalchemy.Column('week_day', sqlalchemy.INTEGER),
        sqlalchemy.Column('begin_time', sqlalchemy.TIME),
        sqlalchemy.Column('end_time', sqlalchemy.TIME),
        sqlalchemy.Column('break_duration', sqlalchemy.INTEGER),
        sqlalchemy.Column('week_order', sqlalchemy.INTEGER),
    )
    op.create_table(
        'special_days',
        sqlalchemy.Column(
            'id',
            sqlalchemy.INTEGER,
            primary_key=True,
            index=True,
        ),
        sqlalchemy.Column('date', sqlalchemy.DATETIME),
        sqlalchemy.Column('is_workday', sqlalchemy.BOOLEAN),
        sqlalchemy.Column('begin_time', sqlalchemy.TIME),
        sqlalchemy.Column('end_time', sqlalchemy.TIME),
        sqlalchemy.Column('break_duration', sqlalchemy.INTEGER),
        sqlalchemy.Column('description', sqlalchemy.TEXT),
        sqlalchemy.Column('day_type', sqlalchemy.INTEGER),
    )
    op.create_table(
        'user_days',
        sqlalchemy.Column(
            'id',
            sqlalchemy.INTEGER,
            primary_key=True,
            index=True,
        ),
        sqlalchemy.Column('date', sqlalchemy.DATETIME),
        sqlalchemy.Column('is_workday', sqlalchemy.BOOLEAN),
        sqlalchemy.Column('begin_time', sqlalchemy.TIME),
        sqlalchemy.Column('end_time', sqlalchemy.TIME),
        sqlalchemy.Column('break_duration', sqlalchemy.INTEGER),
        sqlalchemy.Column('is_plan_day', sqlalchemy.BOOLEAN),
        sqlalchemy.Column('description', sqlalchemy.TEXT),
        sqlalchemy.Column(
            'user_id',
            sqlalchemy.INTEGER,
            sqlalchemy.ForeignKey('users.id'),
            index=True,
        ),
    )


def downgrade():
    op.drop_table('user_days')
    op.drop_table('special_days')
    op.drop_table('standard_days')
