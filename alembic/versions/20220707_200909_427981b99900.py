"""Типу запроса добавлено значение по умолчанию для контента запроса

Revision ID: 427981b99900
Revises: 9ea65058bfbb
Create Date: 2022-07-07 20:09:09.048823

"""
from alembic import op
import sqlalchemy


# revision identifiers, used by Alembic.
revision = '427981b99900'
down_revision = '9ea65058bfbb'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('issue_kinds') as batch_op:
        batch_op.add_column(
            sqlalchemy.Column('default_content', sqlalchemy.String)
        )



def downgrade():
    with op.batch_alter_table('issue_kinds') as batch_op:
        batch_op.drop_column('default_content')
