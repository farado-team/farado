"""Добавлена сущность предметной области: пункт меню

Revision ID: 1ee81c19e6f5
Revises: 41ee450b31ca
Create Date: 2022-06-08 22:20:44.385250

"""
from alembic import op
import sqlalchemy


# revision identifiers, used by Alembic.
revision = '1ee81c19e6f5'
down_revision = '41ee450b31ca'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'menu_items',
        sqlalchemy.Column('id', sqlalchemy.Integer, primary_key=True, index=True),
        sqlalchemy.Column('caption', sqlalchemy.String),
        sqlalchemy.Column('link', sqlalchemy.String),
        sqlalchemy.Column('icon', sqlalchemy.String),
        sqlalchemy.Column('role_id', sqlalchemy.INTEGER, index=True),
    )
    with op.batch_alter_table('menu_items') as batch_op:
        batch_op.create_foreign_key(
            constraint_name='fk_menu_item_role',
            referent_table='roles',
            local_cols=['role_id'],
            remote_cols=['id'],
            ondelete='CASCADE',
        )

def downgrade():
    with op.batch_alter_table('menu_items') as batch_op:
        batch_op.drop_constraint(
            constraint_name='fk_menu_item_role',
            type_="foreignkey",
        )
    op.drop_table('menu_items')
