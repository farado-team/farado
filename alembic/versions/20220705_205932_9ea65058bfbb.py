"""Добавлены веса в состояния рабочего процесса

Revision ID: 9ea65058bfbb
Revises: f1df482531af
Create Date: 2022-07-05 20:59:32.087446

"""
from alembic import op
import sqlalchemy


# revision identifiers, used by Alembic.
revision = '9ea65058bfbb'
down_revision = 'f1df482531af'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('states') as batch_op:
        batch_op.add_column(
            sqlalchemy.Column('weight', sqlalchemy.INTEGER)
        )



def downgrade():
    with op.batch_alter_table('states') as batch_op:
        batch_op.drop_column('weight')
