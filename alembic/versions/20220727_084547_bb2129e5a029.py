"""Добавлена возможность администратору назначить значок для меню роли

Revision ID: bb2129e5a029
Revises: 70032a8a6aeb
Create Date: 2022-07-27 08:45:47.638730

"""
from alembic import op
import sqlalchemy


# revision identifiers, used by Alembic.
revision = 'bb2129e5a029'
down_revision = '70032a8a6aeb'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('roles') as batch_op:
        batch_op.add_column(
            sqlalchemy.Column('icon', sqlalchemy.String)
        )



def downgrade():
    with op.batch_alter_table('roles') as batch_op:
        batch_op.drop_column('icon')
