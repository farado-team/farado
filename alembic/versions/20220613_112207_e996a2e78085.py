"""Добавлена сущность предметной области: сообщение

Revision ID: e996a2e78085
Revises: 1ee81c19e6f5
Create Date: 2022-06-13 11:22:07.222065

"""
from alembic import op
import sqlalchemy


# revision identifiers, used by Alembic.
revision = 'e996a2e78085'
down_revision = '1ee81c19e6f5'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'messages',
        sqlalchemy.Column('id', sqlalchemy.Integer, primary_key=True, index=True),
        sqlalchemy.Column('sender_id', sqlalchemy.INTEGER, sqlalchemy.ForeignKey('users.id')),
        sqlalchemy.Column('receiver_id', sqlalchemy.INTEGER, sqlalchemy.ForeignKey('users.id')),
        sqlalchemy.Column('date_time', sqlalchemy.DateTime),
        sqlalchemy.Column('caption', sqlalchemy.String),
        sqlalchemy.Column('content', sqlalchemy.String),
        sqlalchemy.Column('was_read', sqlalchemy.Boolean, index=True),
    )

def downgrade():
    op.drop_table('messages')
