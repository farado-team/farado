"""Добавлен приоритет для досок

Revision ID: b9d8270bb070
Revises: c579dbdc493a
Create Date: 2024-12-24 15:21:27.461551

"""
from alembic import op
import sqlalchemy


# revision identifiers, used by Alembic.
revision = 'b9d8270bb070'
down_revision = 'c579dbdc493a'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('boards') as batch_op:
        batch_op.add_column(
            sqlalchemy.Column('priority', sqlalchemy.INTEGER)
        )


def downgrade():
    with op.batch_alter_table('boards') as batch_op:
        batch_op.drop_column('priority')
