"""Добавлены значения для типов полей

Revision ID: 70032a8a6aeb
Revises: 427981b99900
Create Date: 2022-07-25 22:01:13.958265

"""
from alembic import op
import sqlalchemy


# revision identifiers, used by Alembic.
revision = '70032a8a6aeb'
down_revision = '427981b99900'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'field_kind_values',
        sqlalchemy.Column(
            'id',
            sqlalchemy.Integer,
            primary_key=True,
            index=True,
        ),
        sqlalchemy.Column(
            'field_kind_id',
            sqlalchemy.INTEGER,
            sqlalchemy.ForeignKey('field_kinds.id'),
            index=True,
        ),
        sqlalchemy.Column(
            'value',
            sqlalchemy.String,
        ),
    )

def downgrade():
    op.drop_table('field_kind_values')
