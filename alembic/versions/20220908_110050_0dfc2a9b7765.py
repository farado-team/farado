"""Состоянию рабочего процесса добавлен порядок и флаг архивного статуса.

Revision ID: 0dfc2a9b7765
Revises: bb2129e5a029
Create Date: 2022-09-08 11:00:50.130822

"""
from alembic import op
import sqlalchemy


# revision identifiers, used by Alembic.
revision = '0dfc2a9b7765'
down_revision = 'bb2129e5a029'
branch_labels = None
depends_on = None



def upgrade():
    with op.batch_alter_table('states') as batch_op:
        batch_op.add_column(
            sqlalchemy.Column('order', sqlalchemy.INTEGER)
        )
        batch_op.add_column(
            sqlalchemy.Column('is_archive', sqlalchemy.BOOLEAN)
        )


def downgrade():
    with op.batch_alter_table('states') as batch_op:
        batch_op.drop_column('order')
        batch_op.drop_column('is_archive')
