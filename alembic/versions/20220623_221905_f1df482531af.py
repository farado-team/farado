"""Добавлена связь сообщения пользователю с изменением запроса.

Revision ID: f1df482531af
Revises: e996a2e78085
Create Date: 2022-06-23 22:19:05.728902

"""
from alembic import op
import sqlalchemy


# revision identifiers, used by Alembic.
revision = 'f1df482531af'
down_revision = 'e996a2e78085'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('messages') as batch_op:
        batch_op.add_column(
            sqlalchemy.Column('issue_change_id', sqlalchemy.INTEGER)
        )
        batch_op.create_foreign_key(
            constraint_name='fk_message_issue_change',
            referent_table='issue_changes',
            local_cols=['issue_change_id'],
            remote_cols=['id'],
            ondelete='CASCADE',
        )



def downgrade():
    with op.batch_alter_table('messages') as batch_op:
        batch_op.drop_constraint(
            constraint_name='fk_message_issue_change',
            type_="foreignkey",
        )
        batch_op.drop_column('issue_change_id')
