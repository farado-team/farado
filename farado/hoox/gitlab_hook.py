#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import json

from farado.general_manager_holder import gm_holder
from farado.editors.issue_editor import IssueEditor

# Для интеграции с GitLab заполните значения следующих переменных:
token = '123456789'
integration_user = 'admin'

# Для того чтобы включить работу данное интеграционное средство необходимо
# убрать комментарий со строки GitLabHook, в контейнере hoox,
# в файле active_hoox.py
class GitLabHook():
    '''Пример обработчика WebHook вызовов от системы GitLab'''

    #--------------------------------------------------------------------------#
    def __init__(
            self,
            args_tuple,
            args_dict,
            request_headers,
            request_body,
        ):
        self.args_tuple = args_tuple
        self.args_dict = args_dict
        self.request_headers = dict(request_headers)
        self.request_body = json.loads(request_body)
        self.token = token
        self.user = gm_holder.project_manager.user_by_login(integration_user)
        self.result_value = ''

    #--------------------------------------------------------------------------#
    def result(self):
        '''[Обязательный метод] Возвращает результат выполнения интеграционного
        взаимодействия.
        '''
        return self.result_value

    #--------------------------------------------------------------------------#
    def is_active(self):
        '''[Обязательный метод] Возвращает флаг, является ли данное
        интеграционное средство активным сейчас (может ли обработать пришедший
        WebHook-вызов).
        '''
        if not len(self.args_tuple):
            return False

        return bool('gitlab' == self.args_tuple[0])

    #--------------------------------------------------------------------------#
    def proceed(self):
        '''[Обязательный метод] Выполняет обработку пришедшего WebHook-вызова 
        '''
        if not(self.token == self.request_headers['X-Gitlab-Token']):
            return False

        event = self.request_headers['X-Gitlab-Event']

        if "Merge Request Hook" == event:
            return self.request_event()

        return False

    #--------------------------------------------------------------------------#
    def request_event(self):
        if not 'merge_request' == self.request_body['event_type']:
            return False

        self.object_attributes = self.request_body['object_attributes']

        if 'merged' == self.object_attributes['state']:
            return self.request_merged()

        return False

    #--------------------------------------------------------------------------#
    def request_merged(self):
        user = self.request_body['user']
        username = user['name']
        email = user['email']

        title = self.object_attributes['title']
        url = self.object_attributes['url']

        merge_commit_sha = self.object_attributes['merge_commit_sha']
        project = self.request_body['project']
        commit_url = project['web_url'] + "/-/commit/" + merge_commit_sha

        args = {
            'new_comment_value':
                f'##### Запрос на слияние: `{title}`\n{url}\n\n' +
                f'##### Коммит слияния: `{merge_commit_sha}`\n{commit_url}\n\n' +
                f'##### Пользователь: `{username}`\n{email}',
        }

        for issue_id in re.findall('[#]\d+', title):
            issue_id = issue_id[1:]

            # TODO: issue_kind_rights
            rights = gm_holder.permission_manager.project_rights(self.user.id)

            result, text, comment_id = IssueEditor(self.user).add_comment(
                rights,
                issue_id,
                args,
                self.user
            )
            self.result_value += f'Результат: {result} {text} {comment_id}\n'
