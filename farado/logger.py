#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import logging.config

def set_config(file_name='resources/logger.cfg'):
    logging.config.fileConfig(file_name)

set_config()

## Main logger
logger = logging.getLogger('farado')
