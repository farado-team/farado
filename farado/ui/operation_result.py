#!/usr/bin/env python
# -*- coding: utf-8 -*-

from farado.editors.editor_result_code import EditResultCode

class OperationResult:
    '''Хранит описание результата выполнения операции

    Атрибуты
    --------
    caption : str
        Наименование результата операции

    text : str
        Текстовое описание результата операции

    kind : str
        Вид результата, используется при световой индикации в ui. Возможные
        значения:
        * primary
        * secondary
        * success
        * danger
        * warning
        * info
        * light
        * dark

    tab_name : str
        Программное наименование вкладки в пользовательском интерфейсе, в рамках
        которой происходила операция.
    '''

    def __init__(
            self,
            caption="",
            text="",
            kind="info",
            result_code=None,
            tab_name=None,
            ):
        self.caption = caption
        self.text = text
        self.tab_name = tab_name

        if result_code is None:
            self.kind = kind
        elif result_code == EditResultCode.none:
            self.kind = "warning"
        else:
            self.kind = "success"
