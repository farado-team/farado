#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cherrypy

from farado.logger import logger
from farado.ui.renderer import view_renderer
from farado.general_manager_holder import project_manager
from farado.general_manager_holder import meta_item_manager, raw_querier
from farado.permission_manager import PermissionFlag
from farado.ui.base_view import BaseView, UiUserRestrictions
from farado.ui.base_view import bring_value, get_value, get_int_value


class SearchView(BaseView):

    #--------------------------------------------------------------------------#
    def __init__(self):
        super().__init__()
        self.name = '/search'

    #--------------------------------------------------------------------------#
    @cherrypy.expose
    def index(self, **args):
        user = self.current_user()
        if not user:
            return self.login_and_open(args=args)

        args = bring_value(user.pop_last_action_data(), args)

        # TODO: вероятно, необходимо доделать права для доступа по проектам
        #       и типам запросов.
        rights = self.project_rights(user.id)
        if PermissionFlag.watcher > rights:
            message = f"Нет доступа к глобальному поиску."
            logger.warn(f"%-18s | {message}", user.login)
            raise cherrypy.HTTPError(403, message)

        # Итоговый перечень объектов-запросов для текущей страницы
        results = []

        # Искомое значение
        search_value = get_value(args, 'search_value', None)

        # Номер текущей страницы (начиная с 1)
        page = get_int_value(args, 'page', 1)

        # Кол-во страниц
        pages = 0

        # Кол-во найденных запросов
        issues_count = 0

        # Кол-во запросов на одной странице
        page_size = 10

        if search_value:
            # Поиск
            search_value = search_value[:40]
            issues_ids = raw_querier().issues_ids_by_caption_or_content(search_value)

            # Пагинация
            issues_count = len(issues_ids)
            pages = issues_count // page_size + (issues_count % page_size > 0)
            index = (page - 1) * page_size
            issues_ids = issues_ids[index: index + page_size]

            # Получение запросов
            results = meta_item_manager().issues_by_ids(issues_ids)

        message = f"Открытие страницы глобального поиска: «{search_value}»."
        logger.info(f"%-18s | {message}", user.login)

        return view_renderer["search"].render(
            user=user,
            project_manager=project_manager(),
            operation_result=user.pop_last_action_result(),
            search_results=results,
            search_value=search_value,
            current_page=page,
            pages=pages,
            issues_count=issues_count,
            restriction=UiUserRestrictions(
                is_admin=self.is_admin(user.id),
            ),
        )
