#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cherrypy

from farado.logger import logger
from farado.ui.renderer import view_renderer
from farado.helpers.cookie_helper import current_session_id
from farado.general_manager_holder import project_manager, permission_manager
from farado.items.issue_kind import IssueKind
from farado.items.field_kind import FieldKind
from farado.items.field_kind_value import FieldKindValue
from farado.ui.operation_result import OperationResult
from farado.ui.base_view import BaseView, UiUserRestrictions
from farado.ui.base_view import bring_value, get_value, get_int_value, get_bool_value


class IssueKindsView(BaseView):

    #--------------------------------------------------------------------------#
    def __init__(self):
        super().__init__()
        self.name = '/issue_kinds'

    #--------------------------------------------------------------------------#
    @cherrypy.expose
    def index(self):
        user = self.current_user()
        if not user:
            return self.login_and_open()

        is_admin = self.is_admin(user.id)
        if not is_admin:
            message = f"Нет доступа к перечню типов запросов."
            logger.warn(f"%-18s | {message}", user.login)
            raise cherrypy.HTTPError(403, message)

        message = f"Открытие страницы с перечнем типов запросов."
        logger.info(f"%-18s | {message}", user.login)

        return view_renderer["issue_kinds"].render(
            user=user,
            project_manager=project_manager(),
            operation_result=user.pop_last_action_result(),
            restriction=UiUserRestrictions(
                is_admin=is_admin,
            )
        )

    #--------------------------------------------------------------------------#
    @cherrypy.expose
    def issue_kind(self, target_issue_kind_id=None, **args):
        user = self.current_user()
        if not user:
            if target_issue_kind_id:
                return self.login_and_open(f'/issue_kind/{target_issue_kind_id}', args)
            return self.login_and_open()

        args = bring_value(user.pop_last_action_data(), args)
        target_issue_kind_caption = get_value(args, 'target_issue_kind_caption', None)
        target_issue_kind_workflow_id = get_int_value(args, 'target_issue_kind_workflow_id', None)
        target_issue_kind_default_state_id = get_int_value(args, 'target_issue_kind_default_state_id', None)
        target_issue_kind_default_content = get_value(args, 'target_issue_kind_default_content', None)

        is_admin = self.is_admin(user.id)
        if not is_admin:
            message = f"Нет доступа к типу запроса #{target_issue_kind_id}."
            logger.warn(f"%-18s | {message}", user.login)
            raise cherrypy.HTTPError(403, message)

        target_issue_kind = project_manager().issue_kind(target_issue_kind_id)

        operation_result = user.pop_last_action_result()
        if target_issue_kind_caption:
            if not target_issue_kind:
                target_issue_kind = IssueKind()
                message = f"Создание типа запроса «{target_issue_kind_caption}»."
                logger.info(f"%-18s | {message}", user.login)

            target_issue_kind.caption = target_issue_kind_caption
            target_issue_kind.workflow_id = int(target_issue_kind_workflow_id)
            target_issue_kind.default_state_id = int(target_issue_kind_default_state_id)
            target_issue_kind.default_content = target_issue_kind_default_content
            project_manager().save_item(target_issue_kind)
            operation_result = OperationResult(
                caption="Тип запроса сохранён",
                kind="success"
            )

            message = f"Тип запроса #{target_issue_kind_id} сохранён."
            logger.info(f"%-18s | {message}", user.login)

        message = f"Открытие страницы типа запроса #{target_issue_kind_id}."
        logger.info(f"%-18s | {message}", user.login)

        return view_renderer["issue_kind"].render(
            user=user,
            target_issue_kind=target_issue_kind,
            project_manager=project_manager(),
            operation_result=operation_result,
            restriction=UiUserRestrictions(
                is_admin=is_admin,
            )
        )

    #--------------------------------------------------------------------------#
    @cherrypy.expose
    def add_issue_kind(self):
        user = self.current_user()
        if not user:
            return self.login_and_open('/add_issue_kind')

        is_admin = self.is_admin(user.id)
        if not is_admin:
            message = f"Нет доступа для создания типа запроса."
            logger.warn(f"%-18s | {message}.", user.login)
            raise cherrypy.HTTPError(403, message)

        message = f"Открытие страницы создания типа запроса."
        logger.info(f"%-18s | {message}", user.login)

        return view_renderer["issue_kind"].render(
            user=user,
            target_issue_kind=None,
            save_result=None,
            project_manager=project_manager(),
            restriction=UiUserRestrictions(
                is_admin=is_admin,
            )
        )

    #--------------------------------------------------------------------------#
    @cherrypy.expose
    def remove_issue_kind(self, target_issue_kind_id):
        user = self.current_user()
        if not user:
            return self.login_and_open(f'/remove_issue_kind/{target_issue_kind_id}')

        is_admin = self.is_admin(user.id)
        if not is_admin:
            message = f"Нет доступа для удаления типа запроса."
            logger.warn(f"%-18s | {message}.", user.login)
            raise cherrypy.HTTPError(403, message)

        project_manager().remove_item(IssueKind, target_issue_kind_id)

        message = f"Тип запроса #{target_issue_kind_id} удалён."
        logger.info(f"%-18s | {message}", user.login)

        user.push_last_action_result(
            OperationResult(
                caption="Тип запроса удалён",
                kind="success"
            )
        )
        raise cherrypy.HTTPRedirect(
            cherrypy.url(f'/')
        )

    #--------------------------------------------------------------------------#
    @cherrypy.expose
    def add_field_kind(self, target_issue_kind_id):
        user = self.current_user()
        if not user:
            return self.login_and_open(f'/add_field_kind/{target_issue_kind_id}')

        is_admin = self.is_admin(user.id)
        if not is_admin:
            message = f"Нет доступа для добавления типа поля типу запроса #{target_issue_kind_id}."
            logger.warn(f"%-18s | {message}.", user.login)
            raise cherrypy.HTTPError(403, message)

        field_kind = FieldKind()
        field_kind.issue_kind_id = int(target_issue_kind_id)
        field_kind.caption = "Новое поле"

        project_manager().save_item(field_kind)

        message = f"Новый тип поля добавлен типу запроса #{target_issue_kind_id}."
        logger.info(f"%-18s | {message}", user.login)

        user.push_last_action_result(
            OperationResult(
                caption="Новый тип поля добавлен типу запроса",
                kind="success",
                tab_name="fields",
            )
        )
        raise cherrypy.HTTPRedirect(
            cherrypy.url(f'/issue_kind/{target_issue_kind_id}')
        )

    #--------------------------------------------------------------------------#
    @cherrypy.expose
    def save_field_kind(self, target_issue_kind_id, **args):
        user = self.current_user()
        if not user:
            return self.login_and_open(f'/save_field_kind/{target_issue_kind_id}', args)

        args = bring_value(user.pop_last_action_data(), args)
        target_field_kind_id = get_int_value(args, 'target_field_kind_id', None)
        target_field_kind_caption = get_value(args, 'target_field_kind_caption', None)
        target_field_kind_description = get_value(args, 'target_field_kind_description', None)
        target_field_kind_value_type = get_int_value(args, 'target_field_kind_value_type', 0)
        target_field_kind_is_system = get_bool_value(args, 'target_field_kind_is_system', 'on', False)
        target_field_kind_values = get_value(args, 'target_field_kind_values', '')

        is_admin = self.is_admin(user.id)
        if not is_admin:
            message = f"Нет доступа для изменения типа поля в типе запроса #{target_issue_kind_id}."
            logger.warn(f"%-18s | {message}.", user.login)
            raise cherrypy.HTTPError(403, message)

        target_field_kind = project_manager().field_kind(target_field_kind_id)
        target_field_kind.caption = target_field_kind_caption
        target_field_kind.description = target_field_kind_description
        target_field_kind.value_type = int(target_field_kind_value_type)
        target_field_kind.is_system = target_field_kind_is_system

        # Изменение перечня возможных значений типа поля
        for value in target_field_kind.values:
            project_manager().remove_item(FieldKindValue, value.id)
        target_field_kind_values = target_field_kind_values.replace(',', '\n').replace('\r', '')
        for field_kind_value in target_field_kind_values.split('\n'):
            if not field_kind_value or field_kind_value == ' ':
                continue
            target_field_kind.values.append(
                FieldKindValue(
                    value=field_kind_value,
                )
            )

        project_manager().save_item(target_field_kind)

        message = f"Тип поля сохранён в типе запроса #{target_issue_kind_id}."
        logger.info(f"%-18s | {message}", user.login)

        user.push_last_action_result(
            OperationResult(
                caption="Тип поля сохранён",
                kind="success",
                tab_name="fields",
            )
        )
        raise cherrypy.HTTPRedirect(
            cherrypy.url(f'/issue_kind/{target_issue_kind_id}')
        )

    #--------------------------------------------------------------------------#
    @cherrypy.expose
    def remove_field_kind(self, target_issue_kind_id, **args):
        user = self.current_user()
        if not user:
            return self.login_and_open(f'/remove_field_kind/{target_issue_kind_id}', args)

        args = bring_value(user.pop_last_action_data(), args)
        target_field_kind_id = get_int_value(args, 'target_field_kind_id', None)

        is_admin = self.is_admin(user.id)
        if not is_admin:
            message = f"Нет доступа для удаления типа поля из типа запроса #{target_issue_kind_id}."
            logger.warn(f"%-18s | {message}.", user.login)
            raise cherrypy.HTTPError(403, message)

        project_manager().remove_item(FieldKind, target_field_kind_id)

        message = f"Тип поля удалён из типа запроса #{target_issue_kind_id}."
        logger.info(f"%-18s | {message}", user.login)

        user.push_last_action_result(
            OperationResult(
                caption="Тип поля удалён из типа запроса",
                kind="success",
                tab_name="fields",
            )
        )
        raise cherrypy.HTTPRedirect(
            cherrypy.url(f'/issue_kind/{target_issue_kind_id}')
        )
