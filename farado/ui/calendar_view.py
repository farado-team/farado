#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cherrypy

from farado.logger import logger
from farado.ui.renderer import view_renderer
from farado.helpers.cookie_helper import current_session_id
from farado.general_manager_holder import project_manager
from farado.items.role import Role
from farado.items.rule import Rule
from farado.items.menu_item import MenuItem
from farado.ui.operation_result import OperationResult
from farado.ui.base_view import BaseView, UiUserRestrictions
from farado.ui.base_view import bring_value, get_value, get_int_value


class CalendarView(BaseView):

    #--------------------------------------------------------------------------#
    def __init__(self):
        super().__init__()
        self.name = '/calendar'

    #--------------------------------------------------------------------------#
    @cherrypy.expose
    def index(self, **args):
        return "todo"

    #--------------------------------------------------------------------------#
    @cherrypy.expose
    def standard_days(self, **args):
        user = self.current_user()
        if not user:
            return self.login_and_open(f'/standard_days', args)

        args = bring_value(user.pop_last_action_data(), args)

        is_admin = self.is_admin(user.id)
        if not is_admin:
            message = f"Нет доступа к настройке календаря."
            logger.warn(f"%-18s | {message}", user.login)
            raise cherrypy.HTTPError(403, message)

        if args:
            for day in project_manager().standard_days():
                day.set_begin_time_string(get_value(args, f'begin_time_{day.week_day}', None))
                day.set_end_time_string(get_value(args, f'end_time_{day.week_day}', None))
                day.set_break_duration_string(get_value(args, f'break_duration_{day.week_day}', None))
                day.week_order = get_int_value(args, f'week_order_{day.week_day}', None)
                project_manager().save_item(day)
            message = f"Изменение настроек календаря."
            logger.info(f"%-18s | {message}", user.login)

        message = f"Открытие страницы настройки календаря."
        logger.info(f"%-18s | {message}", user.login)

        return view_renderer["standard_days"].render(
            user=user,
            project_manager=project_manager(),
            operation_result=user.pop_last_action_result(),
            restriction=UiUserRestrictions(
                is_admin=is_admin,
            )
        )
