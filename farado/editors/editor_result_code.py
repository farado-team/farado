#!/usr/bin/env python
# -*- coding: utf-8 -*-

import enum

class EditResultCode(enum.IntEnum):
    none = 0
    created = 1
    modified = 2
    removed = 3
