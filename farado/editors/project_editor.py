#!/usr/bin/env python
# -*- coding: utf-8 -*-

from farado.items.project import Project
from farado.permission_manager import PermissionFlag
from farado.general_manager_holder import project_manager
from farado.ui.base_view import get_value
from farado.editors.editor_result_code import EditResultCode


class ProjectEditor():

    #--------------------------------------------------------------------------#
    def change(self, rights, project_id=None, **args):
        '''TODO Документарий
        '''
        if PermissionFlag.editor > rights:
            return (EditResultCode.none, 'Нет доступа к изменению проекта', False)

        project = project_manager().project(project_id)
        need_create_new = bool(not project)

        if need_create_new:
            if PermissionFlag.creator > rights:
                return (EditResultCode.none, 'Нет доступа к созданию нового проекта', False)
            project = Project()

        project.caption = get_value(args, 'target_project_caption', project.caption)
        project.content = get_value(args, 'target_project_content', project.content)
        project.issues_view_settings = get_value(
            args,
            'target_project_issues_view_settings',
            project.issues_view_settings,
        )

        project_manager().save_item(project)
        if need_create_new:
            return (EditResultCode.created, 'Новый проект сохранён', project.id)

        return (EditResultCode.modified, 'Проект сохранён', project.id)
