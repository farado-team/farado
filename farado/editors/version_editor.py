#!/usr/bin/env python
# -*- coding: utf-8 -*-

from farado.logger import logger
from farado.general_manager_holder import project_manager
from farado.items.version import Version
from farado.permission_manager import PermissionFlag
from farado.ui.base_view import get_value, get_int_value, get_date_value
from farado.editors.editor_result_code import EditResultCode


class VersionEditor():

    #--------------------------------------------------------------------------#
    def change(self, rights, version_id=None, **args):
        '''TODO Документарий
        '''
        if PermissionFlag.editor > rights:
            return (EditResultCode.none, 'Нет доступа к изменению версии проекта', False)

        version = project_manager().version(version_id)
        need_create_new = bool(not version)

        if need_create_new:
            if PermissionFlag.creator > rights:
                return (EditResultCode.none, 'Нет доступа к созданию новой версии проекта', False)

            version = Version()

        version.caption = get_value(args, 'version_caption', version.caption)
        version.content = get_value(args, 'version_content', version.content)
        version.project_id = get_int_value(args, 'version_project_id', version.project_id)
        version.start_date = get_date_value(args, 'version_start_date', version.start_date)
        version.release_date = get_date_value(args, 'version_release_date', version.release_date)

        project_manager().save_item(version)

        if need_create_new:
            return (EditResultCode.created, 'Новая версия проекта сохранена', version.id)

        return (EditResultCode.modified, 'Версия проекта сохранена', version.id)
