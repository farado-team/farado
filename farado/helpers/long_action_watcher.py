#!/usr/bin/env python
# -*- coding: utf-8 -*-



class LongActionWatcher():
    def __init__(self) -> None:
        self.long_action_handlers = {}

    def push_long_action_handler(self, handler_caption, handler):
        self.stop_long_action_handler(handler_caption)
        self.long_action_handlers[handler_caption] = handler

    def stop_long_action_handler(self, handler_caption):
        if handler_caption in self.long_action_handlers:
            self.long_action_handlers[handler_caption].is_stopped = True

class LongActionHandler():
    is_stopped = False
