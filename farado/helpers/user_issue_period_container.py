#!/usr/bin/env python
# -*- coding: utf-8 -*-

from farado.logger import logger
from farado.helpers.user_issue_period import UserIssuePeriod
from farado.general_manager_holder import project_manager



class UserIssuePeriodContainer:
    def __init__(self):
        # Основные данные
        self.container = []

        # Индексатор <issue_id, [index, index, index ...]>
        self.index = {}

        # Индексатор элементов на удаление
        self.remove_index = []

    def add_start(self, user_id, issue_id, state_id, value):
        # Финализация незакрытых периодов.
        if issue_id in self.index:
            for index in self.index[issue_id]:
                item = self.container[index]
                if not item.end_date_time:
                    item.end_date_time = value

        # Создание нового периода.
        self.container.append(
            UserIssuePeriod(
                user_id=user_id,
                issue_id=issue_id,
                state_id=state_id,
                start_date_time=value,
            )
        )

        # Индексация итогового контейнера по issue_id.
        container_index = len(self.container) - 1
        if issue_id in self.index:
            self.index[issue_id].append(container_index)
        else:
            self.index[issue_id] = [container_index]

    def add_end(self, issue_id, value):
        # Финализация незакрытых периодов.
        if issue_id in self.index:
            for index in self.index[issue_id]:
                item = self.container[index]
                if not item.end_date_time:
                    item.end_date_time = value

    def truncate(self, from_value, to_value):
        # Усечение периодов завершенных до целевого
        self.container = [item
            for item in self.container
            if not item.end_date_time or item.end_date_time > from_value
        ]
        # замена начала периодов, начатых до from_date и добавление
        # незавершённых периодов to_date
        for item in self.container:
            if from_value > item.start_date_time:
                item.start_date_time = from_value
            if not item.end_date_time:
                item.end_date_time = to_value

    def filter_by_user(self, user_id):
        self.container = [
            item
            for item in self.container
            if str(item.user_id) == str(user_id)
        ]

    def container_by_user(self):
        # Формирует упорядоченный перечень пользователей
        result = {
            user.id:[]
            for user in project_manager().ordered_users()
            if user.id in {
                item.user_id
                for item in self.container
                if item.user_id
            }
        }
        # Раскладывает данные по пользователям
        for item in self.container:
            result[item.user_id].append(item)
        return result

    def container_by_projects(self):
        # Формирует упорядоченный перечень проектов
        unique_issues_ids = {
            item.issue_id
            for item in self.container
            if item.issue_id
        }
        projects_issues = {}
        issues_projects = {}
        for issue_id in unique_issues_ids:
            if issue := project_manager().issue(issue_id):
                issues_projects[issue_id] = issue.project_id
                if issue.project_id in projects_issues:
                    projects_issues[issue.project_id].append(issue_id)
                else:
                    projects_issues[issue.project_id] = [issue_id]
        result = {
            project_id:[]
            for project_id in sorted(
                projects_issues.keys(),
                key=lambda project_id: project_manager().project_caption(project_id)
            )
        }

        # Раскладывает данные по проектам
        for item in self.container:
            result[issues_projects[item.issue_id]].append(item)
        return result

    def issues_ids(self):
        return list({item.issue_id for item in self.container})

    def calculate_working_periods(self):
        standard_days = project_manager().standard_days()
        for item in self.container:
            item.set_working_period(standard_days)
