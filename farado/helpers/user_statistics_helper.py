#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime

from farado.logger import logger
from farado.general_manager_holder import gm_holder, raw_querier, project_manager
from farado.database.raw_querier import unite, intersect
from farado.helpers.long_action_watcher import LongActionHandler
from farado.helpers.issue_statistics_helper import states_from_issue_change


class UserStatisticsHelper(LongActionHandler):

    def __init__(self, user) -> None:
        self.user = user

        self.active_states = []
        for state in project_manager().states():
            if state.is_active():
                self.active_states.append(state.id)

    #--------------------------------------------------------------------------#
    def contributions_per_day(self, days=7):
        '''Формирует количество активностей в день за период'''
        pass

    #--------------------------------------------------------------------------#
    def created_issues(self, days=7):
        '''Формирует перечень созданных запросов'''
        pass

    #--------------------------------------------------------------------------#
    def modified_issues(self, days=7):
        '''Формирует перечень запросов, которые изменял'''
        from_date = datetime.datetime.now() - datetime.timedelta(days=days)
        ids = raw_querier().issues_ids_by_modifier_user(self.user.id, from_date)
        issues = gm_holder.meta_item_manager.issues_by_ids(ids)
        return issues

    #--------------------------------------------------------------------------#
    def modified_projects(self, days=7):
        '''Формирует перечень проектов, запросы которых изменял'''
        projects_ids = []
        for issue in self.modified_issues(days):
            if issue.project_id not in projects_ids:
                projects_ids.append(issue.project_id)
        projects = []
        for project_id in projects_ids:
            project = project_manager().project(project_id)
            if project:
                projects.append(project)
        return projects

    #--------------------------------------------------------------------------#
    def modified_versions(self, days=7):
        '''Формирует перечень версий, запросы которых изменял'''
        versions_ids = []
        for issue in self.modified_issues(days):
            if issue.version_id not in versions_ids:
                versions_ids.append(issue.version_id)
        versions = []
        for version_id in versions_ids:
            version = project_manager().version(version_id)
            if version:
                versions.append(version)
        return versions

    #--------------------------------------------------------------------------#
    def issues_comments(self, days=7):
        '''Формирует перечень комментариев к запросам, которые оставлял'''
        pass

    #--------------------------------------------------------------------------#
    def issues_in_progress(self):
        issue_changes_ids = raw_querier().issue_changes_ids_by_user_and_states(
            user_id=self.user.id,
            states_ids=self.active_states,
        )
        issue_changes = project_manager().issue_changes_by_ids(
            issue_changes_ids
        )

        issues_ids = []
        skipped_issues_ids = set()
        for change in issue_changes:
            if change.issue_id in issues_ids:
                continue

            old_state_id, new_state_id = states_from_issue_change(change)
            if not old_state_id or not new_state_id:
                continue

            if not new_state_id in self.active_states:
                skipped_issues_ids.add(change.issue_id)
                continue

            if change.issue_id in skipped_issues_ids:
                continue

            issues_ids.append(change.issue_id)

        # Учитываются только запросы самой глубокой вложенности (листья дерева).
        candidate_issues_ids = []
        for issue_id in issues_ids:
            if not project_manager().sub_issues(issue_id):
                candidate_issues_ids.append(issue_id)

        # Здесь происходит формирование перечня запросов, состояние которых
        # крайний раз менялось именно целевым пользователем. Решает ситуацию,
        # когда над запросом сперва работал один пользователь, а затем он был
        # передан другому.
        result = []
        for issue in project_manager().issues_by_ids(candidate_issues_ids):
            for change in issue.ordered_changes(reverse=True):
                old_state_id, new_state_id = states_from_issue_change(change)
                if not old_state_id or not new_state_id:
                    continue
                if change.user_id == self.user.id:
                    result.append(issue)
                break
        return result
