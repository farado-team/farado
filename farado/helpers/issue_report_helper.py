#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime, timedelta

from farado.logger import logger
from farado.items.issue import Issue
from farado.items.issue_change import IssueChange
from farado.helpers.user_issue_period_container import UserIssuePeriodContainer
from farado.general_manager_holder import project_manager, raw_querier
from farado.helpers.long_action_watcher import LongActionHandler
from farado.helpers.issue_statistics_helper import states_from_issue_change



class IssueReportHelper(LongActionHandler):

    def __init__(self):
        states = project_manager().states()
        self.active_states = [state.id for state in states if state.is_active()]

    #--------------------------------------------------------------------------#
    def active_issues_ids_in_period(self, from_date, to_date):
        from_date = datetime.strptime(from_date, '%d.%m.%Y')
        to_date = datetime.strptime(to_date, '%d.%m.%Y')

        ids = raw_querier().issue_changes_ids_with_state_before_date(to_date)
        issue_changes = project_manager().issue_changes_by_ids(ids)

        issues_states = {}
        for change in issue_changes:
            old_state_id, new_state_id = states_from_issue_change(change)
            if not old_state_id or not new_state_id:
                continue

            if new_state_id in self.active_states:
                issues_states[change.issue_id] = new_state_id
            elif change.date_time < from_date:
                if change.issue_id in issues_states:
                    del issues_states[change.issue_id]

        return list(issues_states.keys())

    #--------------------------------------------------------------------------#
    def active_issues_ids_in_period_by_user(
            self,
            from_date,
            to_date,
            user_id,
            ):
        from_date = datetime.strptime(from_date, '%d.%m.%Y')
        to_date = datetime.strptime(to_date, '%d.%m.%Y')
        to_date += timedelta(seconds=86399)

        ids = raw_querier().issue_changes_ids_with_state_before_date(
            to_date=to_date,
        )
        issue_changes = project_manager().issue_changes_by_ids(ids)

        result = UserIssuePeriodContainer()
        for change in issue_changes:
            current_user_id = change.user_id
            issue_id = change.issue_id
            date_time = change.date_time

            old_state_id, new_state_id = states_from_issue_change(change)
            if not old_state_id or not new_state_id:
                continue

            if new_state_id in self.active_states:
                result.add_start(current_user_id, issue_id, new_state_id, date_time)
            else:
                result.add_end(issue_id, date_time)

        result.truncate(from_date, to_date)
        result.filter_by_user(user_id)
        result.calculate_working_periods()
        return result

    #--------------------------------------------------------------------------#
    def active_issues_ids_in_period_by_project(
            self,
            from_date,
            to_date,
            project_id,
            ):
        from_date = datetime.strptime(from_date, '%d.%m.%Y')
        to_date = datetime.strptime(to_date, '%d.%m.%Y')
        to_date += timedelta(seconds=86399)

        ids = raw_querier().issue_changes_ids_with_state_before_date_by_project(
            to_date=to_date,
            project_id=project_id,
        )
        issue_changes = project_manager().issue_changes_by_ids(ids)

        result = UserIssuePeriodContainer()
        for change in issue_changes:
            user_id = change.user_id
            issue_id = change.issue_id
            date_time = change.date_time

            old_state_id, new_state_id = states_from_issue_change(change)
            if not old_state_id or not new_state_id:
                continue

            if new_state_id in self.active_states:
                result.add_start(user_id, issue_id, new_state_id, date_time)
            else:
                result.add_end(issue_id, date_time)

        result.truncate(from_date, to_date)
        result.calculate_working_periods()
        return result
