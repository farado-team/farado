#!/usr/bin/env python
# -*- coding: utf-8 -*-

import html

def html_escape_over_code_blocks(value, is_first=True):
    result = ''
    if not value:
        return result
    separator = "```" if is_first else "`"
    values = value.split(separator)
    size = len(values)
    for i in range(size):
        part = values[i]
        if bool(i % 2) and size - i > 1:
            part = part.replace('</textarea>', '&lt;/textarea&gt;')
            result += f"{separator}{part}{separator}"
        elif is_first:
            result += html_escape_over_code_blocks(part, False)
        else:
            result += html.escape(part)
    return result
