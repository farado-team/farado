#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime, timedelta

from farado.logger import logger



class UserIssuePeriod:
    def __init__(
            self,
            user_id=None,
            issue_id=None,
            state_id=None,
            start_date_time=None,
            ):
        self.user_id = user_id
        self.issue_id = issue_id
        self.state_id = state_id
        self.start_date_time = start_date_time
        self.end_date_time = None
        self.real_working_period = None
        self.working_days = None

    def from_value(self):
        return f'{self.start_date_time:%Y-%m-%d %H:%M:%S}'

    def to_value(self):
        return f'{self.end_date_time:%Y-%m-%d %H:%M:%S}'

    def working_hours(self):
        if not self.real_working_period:
            return 0
        return round(self.real_working_period.total_seconds() / 3600, 2)

    def set_working_period(self, standard_days):
        self.real_working_period, self.working_days = calculate_working_period(
            self.start_date_time,
            self.end_date_time,
            standard_days
        )

def calculate_working_period(start_date_time, end_date_time, standard_days):
    current_date_time = start_date_time.replace(
        hour=0, minute=0, second=0, microsecond=0
    )
    total_working_period = timedelta(0)
    total_working_days = 0
    step = timedelta(days=1)

    # По всему периоду с шагом в день.
    while current_date_time < end_date_time:
        weekday = current_date_time.weekday()
        standard_day = next(
            (day for day in standard_days if day.week_day == weekday),
            None
        )
        if None == standard_day:
            logger.warning("При построении отчёта не найдено подходящего дня недели!")
            current_date_time += step
            continue

        # Пропуск выходного.
        if standard_day.is_holyday():
            current_date_time += step
            continue

        # Перерыв учитывается только для промежуточных дней,
        # в первый и последний день работ -- не учитывается.
        break_duration = standard_day.break_duration_timedelta()

        current_start_time = standard_day.begin_time
        current_end_time = standard_day.end_time

        # Первый день работ.
        if current_date_time.date() == start_date_time.date():
            current_start_time = start_date_time.time()
            break_duration = timedelta(0)

        # Заключительный день работ.
        if current_date_time.date() == end_date_time.date():
            current_end_time = end_date_time.time()
            break_duration = timedelta(0)

        start = datetime.combine(
            current_date_time.date(), current_start_time
        )
        end = datetime.combine(
            current_date_time.date(), current_end_time
        )
        current_working_period = end - start - break_duration
        if current_working_period > timedelta(0):
            total_working_period += current_working_period
        else:
            pass
            # TODO : решить, что делать в том случае если работы велись
            # в нерабочее время:
            # logger.warn("При построении отчёта получен отрицательный период!")
            # logger.warn(f"current_working_period = {current_working_period}")
            # logger.warn(f"start = {start}")
            # logger.warn(f"end = {end}")
            # logger.warn(f"break_duration = {break_duration}")

        total_working_days += 1
        current_date_time += step

    # То, ради чего всё затевалось.
    return (total_working_period, total_working_days)
