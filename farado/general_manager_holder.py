#!/usr/bin/env python
# -*- coding: utf-8 -*-



class GeneralManagerHolder:
    def set_meta_item_manager(self, meta_item_manager):
        self.meta_item_manager = meta_item_manager

    def set_project_manager(self, project_manager):
        self.project_manager = project_manager

    def set_permission_manager(self, permission_manager):
        self.permission_manager = permission_manager

gm_holder = GeneralManagerHolder()

def project_manager():
    return gm_holder.project_manager

def meta_item_manager():
    return gm_holder.meta_item_manager

def permission_manager():
    return gm_holder.permission_manager

def raw_querier():
    return gm_holder.meta_item_manager.raw_querier
