#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime, timedelta

class StandardDay():
    def __init__(
            self,
            week_day=0,
            begin_time=None,
            end_time=None,
            break_duration=None,
            week_order=0,
            ):
        self.id = None
        self.week_day = week_day
        self.begin_time = begin_time
        self.end_time = end_time
        self.break_duration = break_duration
        self.week_order = week_order

    def set_begin_time_string(self, begin_time):
        self.begin_time = time_string_to_time(begin_time)

    def set_end_time_string(self, end_time):
        self.end_time = time_string_to_time(end_time)

    def set_break_duration_string(self, break_duration):
        self.break_duration = time_string_to_seconds(break_duration)

    def break_duration_string(self):
        hours = int(self.break_duration // 3600)
        minutes = int((self.break_duration % 3600) // 60)
        seconds = int(self.break_duration % 60)
        return f"{hours:02d}:{minutes:02d}:{seconds:02d}"

    def break_duration_timedelta(self):
        return timedelta(seconds=self.break_duration)

    def is_holyday(self):
        return None == self.begin_time or None == self.end_time

    def __repr__(self):
        return str(
            f'''<StandardDay(id='{ self.id
                }',\n week_day='{ self.week_day
                }',\n begin_time='{ self.begin_time
                }',\n end_time='{ self.end_time
                }',\n break_duration='{ self.break_duration
                }',\n week_order='{ self.week_order
                }')>'''
            )

# ---------------------------------------------------------------------------- #

def default_standard_days():
    result = []
    for week_day_number in range(7):
        if week_day_number < 5: # Рабочие дни
            begin_time = time_string_to_time("09:00")
            end_time = time_string_to_time("18:00")
            break_duration = time_string_to_seconds("01:00")
        else: # Выходные
            begin_time = None
            end_time = None
            break_duration = 0

        result.append(
            StandardDay(
                week_day=week_day_number,
                begin_time=begin_time,
                end_time=end_time,
                break_duration=break_duration,
                week_order=week_day_number
            )
        )
    return result

# ---------------------------------------------------------------------------- #

def time_string_to_time(time_str):
    if not time_str:
        return None
    parts_count = len(time_str.split(":"))
    if parts_count == 2:
         return datetime.strptime(time_str, "%H:%M").time()
    if parts_count == 3:
         return datetime.strptime(time_str, "%H:%M:%S").time()
    return None

def time_string_to_seconds(time_str):
    if not time_str:
        return 0
    parts = time_str.split(':')
    if len(parts) == 2:
        hours, minutes = map(int, parts)
        return int(timedelta(hours=hours, minutes=minutes).total_seconds())
    if len(parts) == 3:
        hours, minutes, seconds = map(int, parts)
        return int(timedelta(hours=hours, minutes=minutes, seconds=seconds).total_seconds())
    return 0
