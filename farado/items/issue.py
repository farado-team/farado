#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Issue():
    def __init__(
            self,
            caption=None,
            content=None,
            issue_kind_id=None,
            parent_id=None,
            project_id=None,
            state_id=None,
            version_id=None):
        self.id = None
        self.caption = caption
        self.content = content
        self.issue_kind_id = issue_kind_id
        self.parent_id = parent_id
        self.project_id = project_id
        self.state_id = state_id
        self.version_id = version_id
        # Поля создаваемые sqlalchemy.orm.mapper:
        # self.fields = []
        # self.files = []
        # self.comments = []
        # self.changes = []

    def __repr__(self):
        return str(
            f'''<Issue(id='{ self.id
                }',\n caption='{ self.caption
                }',\n content='{ self.content
                }',\n issue_kind_id='{ self.issue_kind_id
                }',\n parent_id='{ self.parent_id
                }',\n project_id='{ self.project_id
                }',\n state_id='{ self.state_id
                }',\n version_id='{ self.version_id
                }')>'''
            )

    def field(self, field_kind_id):
        if not field_kind_id:
            return None
        field_kind_id = int(field_kind_id)
        for field in self.fields:
            if field_kind_id == field.field_kind_id:
                return field
        return None

    def file(self, file_id):
        if not file_id:
            return None
        file_id = int(file_id)
        for file in self.files:
            if file_id == file.id:
                return file
        for comment in self.comments:
            for file in comment.files:
                if file_id == file.id:
                    return file
        return None

    def comment(self, comment_id):
        if not comment_id:
            return None
        comment_id = int(comment_id)
        for comment in self.comments:
            if comment_id == comment.id:
                return comment
        return None

    def ordered_comments(self):
        return sorted(
            self.comments,
            key=lambda comment: comment.creation_datetime)

    def ordered_changes(self, reverse=False):
        return sorted(
            self.changes,
            reverse=reverse,
            key=lambda change: change.date_time)

    def created(self):
        changes = self.ordered_changes()
        if changes:
            return f'{changes[0].date_time:%d.%m.%Y %H:%M:%S}'
        return ''

    def created_date_time(self):
        changes = self.ordered_changes()
        if changes:
            return changes[0].date_time
        return None

    def create_user_id(self):
        changes = self.ordered_changes()
        if changes:
            return changes[0].user_id
        return None

    def last_modified(self):
        changes = self.ordered_changes()
        if changes:
            return f'{changes[-1].date_time:%d.%m.%Y %H:%M:%S}'
        return ''

    def last_modified_by_user(self, user_id):
        user_id = int(user_id)
        changes = self.ordered_changes(reverse=True)
        for change in changes:
            if int(change.user_id) == user_id:
                return f'{change.date_time:%d.%m.%Y %H:%M:%S}'
        return ''

    def last_modify_user_id(self):
        changes = self.ordered_changes()
        if changes:
            return changes[-1].user_id
        return None
