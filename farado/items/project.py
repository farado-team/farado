#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Project():
    def __init__(
            self,
            caption='',
            content='',
            issues_view_settings='',
            ):
        self.id = None
        self.caption = caption
        self.content = content
        self.issues_view_settings = issues_view_settings
        # Поля создаваемые sqlalchemy.orm.mapper:
        # self.issues = []
        # self.versions = []

    def __repr__(self):
        return str(
            f'''<Project(id='{ self.id
                }',\n caption='{ self.caption
                }',\n content='{ self.content
                }',\n issues_view_settings='{ self.issues_view_settings
                }')>'''
        )

    # TODO : Отказаться от метода, когда будет ui «с защитой от дурака» для формирования столбцов
    def get_issues_view_settings(self):
        if not self.issues_view_settings:
            return 'id,kind,state,caption,parent'
        return self.issues_view_settings
