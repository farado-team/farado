#!/usr/bin/env python
# -*- coding: utf-8 -*-

import enum
import datetime

class VersionStates(enum.IntEnum):
    invalid = 0
    not_started = 1
    in_progress = 2
    released = 3



class Version():
    def __init__(
            self,
            caption=None,
            content=None,
            project_id=None,
            start_date=datetime.datetime(1970, 1, 1),
            release_date=datetime.datetime(1970, 1, 1),
            ):
        self.id = None
        self.caption = caption
        self.content = content
        self.project_id = project_id
        self.start_date = start_date
        self.release_date = release_date
        # Поля создаваемые sqlalchemy.orm.mapper:
        # self.issues = []

    def __repr__(self):
        return str(
            f'''<Version(id='{ self.id
                }',\n caption='{ self.caption
                }',\n content='{ self.content
                }',\n project_id='{ self.project_id
                }',\n start_date='{ self.start_date
                }',\n release_date='{ self.release_date
                }')>'''
            )

    def version_states(self):
        return VersionStates

    def state(self):
        if self.start_date > self.release_date:
            return VersionStates.invalid

        current_date = datetime.datetime.now().date()
        if current_date < self.start_date:
            return VersionStates.not_started

        if current_date > self.release_date:
            return VersionStates.released

        return VersionStates.in_progress

    def is_pending_state(self):
        current_state = self.state()
        return bool(
            VersionStates.not_started == current_state
            or
            VersionStates.in_progress == current_state
        )
