#!/usr/bin/env python
# -*- coding: utf-8 -*-

import enum
import datetime

from farado.helpers.diff_helper import diff_to_json, json_to_diff
from farado.helpers.diff_helper import text_changes, DiffLineType



class IssueChange():
    class DiffKey(enum.IntEnum):
        caption = 1,
        content = 2,
        state = 3,
        project = 4,
        parent = 5,
        version = 6,
        files = 7,
        fields = 8,
        comments = 9,

    def __init__(
            self,
            issue_id=None,
            json_diff=None,
            dict_diff=None,
            user_id=None,
            date_time=None
            ):
        self.id = None
        self.issue_id = issue_id
        self.diff = json_diff if json_diff else diff_to_json(dict_diff)
        self.user_id = user_id
        self.date_time = date_time
        if not self.date_time:
            self.date_time = datetime.datetime.now()

    def __repr__(self):
        return str(
            f'''<IssueChanges(id='{ self.id
                }',\n issue_id='{ self.issue_id
                }',\n diff='{ self.diff
                }',\n user_id='{ self.user_id
                }',\n date_time='{ self.date_time
                }')>'''
            )

    def formated_date_time(self):
        # TODO: Вынести в конфиг формат даты/времени
        return f'{self.date_time:%d.%m.%Y %H:%M:%S}'

    def dict_diff(self):
        return json_to_diff(self.diff, IssueChange.DiffKey)

    def json_diff(self):
        return self.diff

    # TODO: здесь некрасивый проброс функции для ui, сделать красиво)
    def text_changes(self, value):
        return text_changes(value)

    def diff_line_type(self):
        return DiffLineType
