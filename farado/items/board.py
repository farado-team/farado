#!/usr/bin/env python
# -*- coding: utf-8 -*-

import enum
import json


class Board():
    class FilterKey(enum.IntEnum):
        '''Ключи фильтра.
        * kinds_ids — фильтрация по типам запросов
        * projects_ids — фильтрация по проектам
        * versions_ids — фильтрация по версиям
        '''
        kinds_ids = 1,
        projects_ids = 2,
        versions_ids = 3,

    #--------------------------------------------------------------------------#
    def __init__(
            self,
            caption=None,
            description=None,
            workflow_id=None,
            filter_json=None,
            priority=None,
            ):
        self.id = None
        self.caption = caption
        self.description = description
        self.workflow_id = workflow_id
        self.filter_json = filter_json
        self.priority = priority
        # Поля создаваемые sqlalchemy.orm.mapper:
        # self.board_columns = []

    #--------------------------------------------------------------------------#
    def __repr__(self):
        return str(
            f'''<Board(id='{ self.id
                }',\n caption='{ self.caption
                }',\n description='{ self.description
                }',\n workflow_id='{ self.workflow_id
                }',\n filter_json='{ self.filter_json
                }',\n priority='{ self.priority
                }')>'''
        )

    #--------------------------------------------------------------------------#
    def board_column(self, board_column_id):
        if not board_column_id:
            return None
        board_column_id = int(board_column_id)
        for board_column in self.board_columns:
            if board_column_id == board_column.field_kind_id:
                return board_column
        return None

    #--------------------------------------------------------------------------#
    def ordered_board_columns(self):
        return sorted(
            self.board_columns,
            key=lambda column: column.order)

    #--------------------------------------------------------------------------#
    def filter(self):
        '''Парсит json-строку с данными фильтра и возвращает соответствующий
        словарь.

        Возвращает
        ----------
        dict
            Словарь с данными фильтрации запросов (issue) для данной доски.
            Ключ — Board.FilterKey, значение — лист идентификаторов сущностей
            в зависимости от ключа.
        '''
        if not self.filter_json:
            return {}

        result = json.loads(self.filter_json)

        # Типизация ключей первого уровня по FilterKey
        for key in Board.FilterKey:
            json_key = str(int(key))
            if json_key in result:
                value = result.pop(json_key)
                if list == type(value):
                    result[key] = [int(item) for item in value if item and int(item)]
                else:
                    result[key] = value
        return result

    #--------------------------------------------------------------------------#
    def filter_by_key(self, key):
        '''Формирует и возвращает лист идентификаторов сущностей в зависимости
        от переданного ключа.

        Параметры
        ----------
        key : Board.FilterKey
            Элемент перечисления, фильтр каких сущностей нужно вернуть.

        Возвращает
        ----------
        list
            Лист идентификаторов сущностей в зависимости от ключа.
        '''
        filter = self.filter()
        if key in filter:
            return filter[key]
        return []

    #--------------------------------------------------------------------------#
    def filter_key(self):
        return Board.FilterKey

    #--------------------------------------------------------------------------#
    def set_filter_by_key(self, key, value):
        '''Сохраняет в поле фильтра объекта новый перечень идентификаторов
        сущностей в зависимости от указанного ключа.

        Параметры
        ----------
        key : Board.FilterKey
            Элемент перечисления, фильтр каких сущностей нужно сохранить.

        value : list
            Лист идентификаторов.
        '''
        if type(value) == str and "[" in value and "]" in value:
            value = eval(value)

        if not type(value) == list:
            value = [value]

        filter = self.filter()
        if (value == [0]) and (key in filter):
            del filter[key]
        else:
            filter[key] = value

        self.filter_json = json.dumps(filter, indent=2)
