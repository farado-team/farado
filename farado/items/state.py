#!/usr/bin/env python
# -*- coding: utf-8 -*-

class State():
    def __init__(
            self,
            caption="",
            description="",
            workflow_id=None,
            weight=0,
            order=0,
            is_archive=0,
            ):
        self.id = None
        self.caption = caption
        self.description = description
        self.workflow_id = workflow_id
        self.weight = weight
        self.order = order
        self.is_archive = is_archive

    def __repr__(self):
        return str(
            f'''<State(id='{ self.id
                }',\n caption='{ self.caption
                }',\n description='{ self.description
                }',\n workflow_id='{ self.workflow_id
                }',\n weight='{ self.weight
                }',\n order='{ self.order
                }',\n is_archive='{ self.is_archive
                }')>'''
            )

    def weight_percentage(self):
        if not self.weight or 0 > int(self.weight):
            return 0
        if 100 < int(self.weight):
            return 100
        return int(self.weight)

    def is_active(self):
        percentage = self.weight_percentage()
        return bool((0 < percentage) and (100 > percentage))

    def is_not_started(self):
        percentage = self.weight_percentage()
        return bool(0 == percentage)

    def is_finished(self):
        percentage = self.weight_percentage()
        return bool(100 == percentage)
