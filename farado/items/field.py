#!/usr/bin/env python
# -*- coding: utf-8 -*-

from farado.items.field_kind import ValueTypes

class Field():
    def __init__(
            self,
            value=None,
            field_kind_id=None,
            issue_id=None
            ):
        self.id = None
        self.value = value
        self.field_kind_id = field_kind_id
        self.issue_id = issue_id

    def __repr__(self):
        return str(
            f'''<Field(id='{ self.id
                }',\n issue_id='{ self.issue_id
                }',\n field_kind_id='{ self.field_kind_id
                }',\n value='{ self.value
                }')>'''
        )

    def set_value(self, value, value_type):
        if list == type(value):
            if value_type == ValueTypes.select_multi:
                self.set_value_as_list(value)
            else:
                self.set_value_as_ids(value)
        else:
            if value_type == ValueTypes.select:
                self.value = '' if value == '0' else value
            elif value_type == ValueTypes.select_multi:
                self.value = '' if value == '0' else value
            else:
                self.value = value

    def value_as_ids(self):
        if str == type(self.value):
            return [int(item) for item in self.value.split(',')]
        return []

    def set_value_as_ids(self, value):
        self.value = ','.join([str(item) for item in value if int(item)])

    def value_as_list(self):
        if str == type(self.value):
            return [item for item in self.value.split(',')]
        return []

    def set_value_as_list(self, value):
        self.value = ','.join([item for item in value if item and not item == '0'])

