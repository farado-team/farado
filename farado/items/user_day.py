#!/usr/bin/env python
# -*- coding: utf-8 -*-

class UserDay():
    def __init__(
            self,
            date=None,
            is_workday=None,
            begin_time=None,
            end_time=None,
            break_duration=None,
            is_plan_day=None,
            description='',
            user_id=None,
            ):
        self.id = None
        self.date = date
        self.is_workday = is_workday
        self.begin_time = begin_time
        self.end_time = end_time
        self.break_duration = break_duration
        self.is_plan_day = is_plan_day
        self.description = description
        self.user_id = user_id

    def __repr__(self):
        return str(
            f'''<UserDay(id='{ self.id
                }',\n date='{ self.date
                }',\n is_workday='{ self.is_workday
                }',\n begin_time='{ self.begin_time
                }',\n end_time='{ self.end_time
                }',\n break_duration='{ self.break_duration
                }',\n is_plan_day='{ self.is_plan_day
                }',\n description='{ self.description
                }',\n user_id='{ self.user_id
                }')>'''
            )
