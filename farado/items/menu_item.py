#!/usr/bin/env python
# -*- coding: utf-8 -*-

class MenuItem():
    def __init__(
            self,
            caption='',
            link='',
            icon='',
            role_id=None,
            ):
        self.id = None
        self.caption = caption
        self.link = link
        self.icon = icon
        self.role_id = role_id

    def __repr__(self):
        return str(
            f'''<MenuItem(id='{ self.id
                }',\n caption='{ self.caption
                }',\n link='{ self.link
                }',\n icon='{ self.icon
                }',\n role_id='{ self.role_id
                }')>'''
            )
