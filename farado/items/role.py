#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Role():
    def __init__(
            self,
            caption='',
            icon=None,
            ):
        self.id = None
        self.caption = caption
        self.icon = icon
        # Поля создаваемые sqlalchemy.orm.mapper:
        # self.rules = []
        # self.menu_items = []

    def __repr__(self):
        return str(
            f'''<Role(id='{ self.id
                }',\n caption='{ self.caption
                }',\n icon='{ self.icon
                }')>'''
            )

    def menu_item(self, menu_item_id):
        if not menu_item_id:
            return None
        menu_item_id = int(menu_item_id)
        for menu_item in self.menu_items:
            if menu_item_id == menu_item.id:
                return menu_item
        return None
