#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime

class Message():
    def __init__(
            self,
            sender_id=None,
            receiver_id=None,
            date_time=None,
            caption=None,
            content=None,
            was_read=False,
            issue_change_id=None):
        self.id = None
        self.sender_id = sender_id
        self.receiver_id = receiver_id
        self.caption = caption
        self.content = content
        self.was_read = was_read
        self.issue_change_id = issue_change_id
        self.date_time = date_time
        if not self.date_time:
            self.date_time = datetime.datetime.now()

    def __repr__(self):
        return str(
            f'''<Message(id='{ self.id
                }',\n sender_id='{ self.sender_id
                }',\n receiver_id='{ self.receiver_id
                }',\n date_time='{ self.date_time
                }',\n caption='{ self.caption
                }',\n content='{ self.content
                }',\n was_read='{ self.was_read
                }',\n issue_change_id='{ self.issue_change_id
                }')>'''
            )

    def formated_date_time(self):
        # TODO: Добавить в конфиг параметр для форматирования даты и времени
        return f'{self.date_time:%d.%m.%Y %H:%M:%S}'
