#!/usr/bin/env python
# -*- coding: utf-8 -*-

import enum

class SpecialDay():
    class DayType(enum.IntEnum):
        public_holiday = 1,         # Государственный праздник
        short_working_day = 2,      # Короткий рабочий день
        relocated_holiday = 3,      # Перенесённый выходной
        relocated_working_day = 4,  # Перенесённый рабочий

    def __init__(
            self,
            date=None,
            is_workday=None,
            begin_time=None,
            end_time=None,
            break_duration=None,
            description='',
            day_type=None,
            ):
        self.id = None
        self.date = date
        self.is_workday = is_workday
        self.begin_time = begin_time
        self.end_time = end_time
        self.break_duration = break_duration
        self.description = description
        self.day_type = day_type

    def __repr__(self):
        return str(
            f'''<SpecialDay(id='{ self.id
                }',\n date='{ self.date
                }',\n is_workday='{ self.is_workday
                }',\n begin_time='{ self.begin_time
                }',\n end_time='{ self.end_time
                }',\n break_duration='{ self.break_duration
                }',\n description='{ self.description
                }',\n day_type='{ self.day_type
                }')>'''
            )
