#!/usr/bin/env python
# -*- coding: utf-8 -*-

class FieldKindValue():
    def __init__(
            self,
            field_kind_id=None,
            value=None,
            ):
        self.id = None
        self.field_kind_id = field_kind_id
        self.value = value

    def __repr__(self):
        return str(
            f'''<FieldKindValue(id='{ self.id
                }',\n field_kind_id='{ self.field_kind_id
                }',\n value='{ self.value
                }')>'''
            )
