#!/usr/bin/env python
# -*- coding: utf-8 -*-

import enum

from farado.logger import logger
from farado.items.user import User
from farado.session_manager import SessionManager
from farado.general_manager_holder import gm_holder


class PermissionFlag(enum.IntEnum):
    none = 0
    watcher = 1
    editor = 2
    creator = 4
    deleter = 8

# TODO: Перевод логов.
class PermissionManager:

    def __init__(self):
        self.session_manager = SessionManager()

    def login(self, username, password):
        if not username or not password:
            logger.warn(f'Ошибка авторизации — нет ни логина, ни пароля.')
            return False

        user = gm_holder.project_manager.user_by_login(username)
        if not user:
            logger.warn(f'%-18s | Ошибка авторизации — нет такого пользователя.', username)
            return False

        if user.is_blocked:
            logger.warn(f'%-18s | Ошибка авторизации — пользователь заблокирован.', username)
            return False

        if not user.check_password(password):
            logger.warn(f'%-18s | Ошибка авторизации — неверный пароль.', username)
            return False

        session_id = self.session_manager.create_session(user)
        if session_id:
            logger.info(f'%-18s | Успешный вход в систему, сессия: {session_id}.', username)

        return session_id

    def logout(self, session_id):
        if not session_id:
            logger.warn(f'Ошибка выхода — сессия не задана.')
            return

        user = self.session_manager.user_by_session_id(session_id)
        if user:
            logger.info(f'%-18s | Выполнен выход из системы, сессия: {session_id}.', user.login)
        else:
            logger.info(f'Выполнен выход из системы, сессия: {session_id}.')

        self.session_manager.remove_session(session_id)

    def user_by_session_id(self, session_id):
        return self.session_manager.user_by_session_id(session_id)

    def project_rights(self, user_id, project_id=None):
        if project_id:
            project_id = int(project_id)
        result = 0
        for role in gm_holder.project_manager.roles_by_user(user_id):
            for rule in role.rules:
                if rule.project_id and not(rule.project_id == project_id):
                    continue
                if rule.project_rights:
                    result |= rule.project_rights
        return result

    def check_is_admin(self, user_id):
        for role in gm_holder.project_manager.roles_by_user(user_id):
            for rule in role.rules:
                if rule.is_admin:
                    return True
        return False
