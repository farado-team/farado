#!/usr/bin/env python
# -*- coding: utf-8 -*-

from farado.general_manager_holder import meta_item_manager

from farado.items.field_kind import FieldKind
from farado.items.state import State
from farado.items.edge import Edge
from farado.items.version import Version
from farado.items.issue_kind import IssueKind



class ItemMapper:
    def field_kinds_ids_by_issue_kind_id(self, id):
        return meta_item_manager().items_ids_by_value(FieldKind, "issue_kind_id", int(id))

    def states_ids_by_workflow_id(self, id):
        return meta_item_manager().items_ids_by_value(State, "workflow_id", int(id))

    def edges_ids_by_workflow_id(self, id):
        return meta_item_manager().items_ids_by_value(Edge, "workflow_id", int(id))

    def issue_kinds_ids_by_workflow_id(self, id):
        return meta_item_manager().items_ids_by_value(IssueKind, "workflow_id", int(id))

    def versions_ids_by_project_id(self, id):
        return meta_item_manager().items_ids_by_value(Version, "project_id", int(id))
